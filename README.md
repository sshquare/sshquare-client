# sshquare client

Once you have configured your [sshquare device](https://gitlab.com/juanitobananas/sshquare-device/#sshquare-device), you should be able to access it with a normal ssh client using the settings you will find in sshquare.io.

Here you have an example of an ssh command:

```
ssh -p 10000 yourdeviceuser@sshquare.io
```

You could also use the [configure-sshquare-client script](/configure-sshquare-client) to add your sshquare.io hosts to your ~/.ssh/config file which would allow you to connect to your sshquare.io device with a somewhat simpler command:

```
ssh yourdeviceuser@yourdevicename.sshquare.io
```

## Using the configure-sshquare-client script

The [script](/configure-sshquare-client) adds all your sshquare.io devices to your ~/.ssh/config.

You do not need to run this with super user privileges.

```
cd /tmp
wget "https://gitlab.com/juanitobananas/sshquare-client/raw/master/configure-sshquare-client"
chmod +x configure-sshquare-client
./configure-sshquare-client
rm configure-sshquare-client
```
